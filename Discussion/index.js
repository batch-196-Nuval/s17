/*alert("Gwapo!");*/

// Functions
/*
	Syntax:
		function functionName (){
			code block(staement)
		};
	function - tells JS that this is a function
	functionName - Name of function
	code of block - set of instructions
	function needs to be invoked or called
*/

function printName(){
	console.log('My name is Anna');
};

// invoke / call the function
printName(); // result: My name is Anna

/*declaredFunction(); // result: err - not yet defined*/

// Function Declaration versus Expressions

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction()");
};

declaredFunction();


// Function Expression

	// Anonymous function - function without a name

let variableFunction = function(){
	console.log('I am from variableFunction');
};

variableFunction();

let funcExpression = function funcName(){
	console.log('Hello from the other side');
};

funcExpression();

// You can reassign declared functions and function expression to new anonymous function

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log('Updated funcExpression');
};

funcExpression();

const constantFunction = function(){
	console.log('Initialized with const');
};

constantFunction();

/*constantFunction = function(){
	console.log('Cannot be reassigned!');
};

constantFunction();*/
// reassignment with const function expression is not possible

// Function Scoping
/*
	JavaScript Variables has 3 types of scope:
	1. local / block scope
	2. global scope
	3. function scope
*/

{
	let localVar = "Armando Perez";
}

let globalVar = "Mr. Worldwide";

console.log(globalVar);
/*console.log(localVar); // result: err*/

function showNames(){
	// function scope variable
	var functionVar = 'Joe';
	const functionConst = 'Nick';
	let functionLet = 'Kevin';

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();
/*
console.log(functionVar);
console.log(functionConst);
console.log(functionLet);
*/

// Nested Function

function myNewFunction(){
	let name = 'Yor';

	function nestedFunction(){
		let nestedName = 'Brando';
		console.log(nestedName);
	};

	nestedFunction();
};

myNewFunction();

// Function and Global Scoped Variable

// Global Scope variable
let globalName = 'Alan';

function myNewFunction2 (){
	letnameInside = "Marco";
	console.log(globalName);
};

myNewFunction2();


// Alert

/*
	Syntax:
		alert('message');

*/

/*alert("Gwapo mo naman!");*/

function showSampleAlert(){
	alert('Hello Gwapo!');
};

/*showSampleAlert();*/

console.log("I will only log in the console when the alert is dismissed.");

// prompt()

/*
	Syntax:
		prompt("dialog");

*/

/*let samplePrompt = prompt("Enter your name, gwapo: ");
console.log("Hello " + samplePrompt);

let sampleNullPrompt = prompt("Don't Enter Anything!");
console.log(sampleNullPrompt);*/
// if the prompt() is cancelled, the result will be: null
// if there is no input in the prompt, the result will be: empty string

function printWelcomeMessage(){
	let firstName = prompt('Enter your First Name: ');
	let lastName = prompt('Enter your Last Name: ');

	console.log('Hello, ' + firstName + " " + lastName + "!");
	console.log('Welcome to my page!');
};

printWelcomeMessage();

// Function Naming Conventions

function getCourses(){
	let courses = ['Science 101', 'Arithmetic 103', 'Grammar 105'];
	console.log(courses);
};

getCourses();

// Avoid generic names to avoid confusion

function get(){
	let name = 'Anya';
	console.log(name);
};

get();

// Avoid pointless and inappropriate function names

function foo(){
	console.log(25 % 5);
};

foo();

// Name your functions in small caps. Follow camelcase when naming functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Prince: 1,500,00");
};

displayCarInfo();



