/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function promptInfo(){
		let fullName = prompt("Please provide your Full Name (First Name Last Name): ");
		let age = prompt("Kindly state your Age: ");
		let location = prompt("Please share your current Location: ");
		console.log("Hi " + fullName);
		console.log(fullName + "'s age is " + age);
		console.log(fullName + " is located at " + location);
	};
	promptInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayTopBands(){
		let topBands = ["Nirvana", "Guns 'N Roses", "Aerosmith", "Eraserheads", "Rivermaya"];
		console.log(topBands);
	}
	displayTopBands();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function displayTopMovies(){
			let TopOneMovie = "Avengers: Endgame";
			let TopOneMovieTomato = "94%";
			let TopTwoMovie = "Avengers: Infinity War";
			let TopTwoMovieTomato = "85%";
			let TopThreeMovie = "Marvel's The Avengers";
			let TopThreeMovieTomato = "91%";
			let TopFourMovie = "Captain America: Civil War";
			let TopFourMovieTomato = "90%";
			let TopFiveMovie = "Thor: Ragnarok";
			let TopFiveMovieTomato = "93%";
			console.log("1. " + TopOneMovie);
			console.log("Tomatometer for " + TopOneMovie + ": " + TopOneMovieTomato);
			console.log("2. " + TopTwoMovie);
			console.log("Tomatometer for " + TopTwoMovie + ": " + TopTwoMovieTomato);
			console.log("3. " + TopThreeMovie);
			console.log("Tomatometer for " + TopThreeMovie + ": " + TopThreeMovieTomato);
			console.log("4. " + TopFourMovie);
			console.log("Tomatometer for " + TopFourMovie + ": " + TopFourMovieTomato);
			console.log("5. " + TopFiveMovie);
			console.log("Tomatometer for " + TopFiveMovie + ": " + TopFiveMovieTomato);
	};
	displayTopMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	let printFriends = function printUsers() {
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name: "); 
		let friend2 = prompt("Enter your second friend's name: "); 
		let friend3 = prompt("Enter your third friend's name: ");

		console.log("You are friends with:");
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};
	printFriends();

/*console.log(friend1);
console.log(friend2);*/